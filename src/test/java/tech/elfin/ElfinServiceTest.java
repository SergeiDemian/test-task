package tech.elfin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;
import tech.elfin.models.Answer;
import tech.elfin.models.Request;
import tech.elfin.models.RiskFactor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
public class ElfinServiceTest {


    @TestConfiguration
    static class TestElfinService {

        @Autowired
        private ResourceLoader resourceLoader;

        @Bean
        public ElfinService elfinService() {
            return new ElfinService(createDmnEngine(), createDecision());
        }

        @Bean
        public DmnEngine createDmnEngine() {
            DmnEngineConfiguration configuration = DmnEngineConfiguration
                    .createDefaultDmnEngineConfiguration();
            return configuration.buildEngine();
        }

        @Bean(name = "dmn_decision")
        public DmnDecision createDecision() {
            Resource resource = resourceLoader.getResource("classpath:dmn/test_task.dmn");
            InputStream is = null;
            try {
                is = resource.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return createDmnEngine().parseDecision("TestTable", is);
        }
    }

    @Autowired
    ElfinService elfinService;

    private Answer answer;

    private Request request;

    @Before
    public void setUp() throws JsonProcessingException {
        List<RiskFactor> risks = new ArrayList<>();
        RiskFactor riskFactor = new RiskFactor();
        riskFactor.setFactor("settl true status");
        risks.add(riskFactor);
        request = new Request();
        request.setCompanyName("sun");
        request.setRiskFactors(risks);
        answer = new Answer();
        answer.setStatus(true);
        answer.setAdditional(new ArrayList<String>
                (Collections.singleton("Состояние расчетов по налогам: Клиент в списке юридических лиц, " +
                        "имеющих задолженность по уплате налогов и/или не представляющих налоговую отчетность" +
                        " более года, отсутствует;")));
        ObjectMapper objectMapper = new ObjectMapper();
    }

    @Test
    public void validateWithFullRequest() {
        assertEquals(elfinService.validate(request), answer);
    }

    @Test
    public void validateWithoutRiskFactor() {
        request.setRiskFactors(new ArrayList<>());
        answer.setStatus(false);
        answer.setAdditional(null);
        assertEquals(elfinService.validate(request), answer);
    }
}

