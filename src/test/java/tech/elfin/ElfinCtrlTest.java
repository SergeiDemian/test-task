package tech.elfin;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tech.elfin.models.Answer;
import tech.elfin.models.Request;
import tech.elfin.models.RiskFactor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ElfinCtrl.class)
class ElfinCtrlTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ElfinService elfinService;

    private Answer answer;

    private String json;

    private Request request;

    @BeforeEach
    public void setUp() throws Exception {
        List<RiskFactor> risks = new ArrayList<>();
        RiskFactor riskFactor = new RiskFactor();
        riskFactor.setFactor("settl true status");
        risks.add(riskFactor);
        request = new Request();
        request.setCompanyName("sun");
        request.setRiskFactors(risks);
        answer = new Answer();
        answer.setStatus(true);
        answer.setAdditional(new ArrayList<String>
                (Collections.singleton("Состояние расчетов по налогам: Клиент в списке юридических лиц, " +
                        "имеющих задолженность по уплате налогов и/или не представляющих налоговую отчетность" +
                        " более года, отсутствует;")));
        ObjectMapper objectMapper = new ObjectMapper();
        json = objectMapper.writeValueAsString(request);
    }

    @Test
    void validate() throws Exception {
        Mockito.when(elfinService.validate(request)).thenReturn(answer);
        this.mockMvc.perform(post("/scoring")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().is2xxSuccessful());
    }
}
