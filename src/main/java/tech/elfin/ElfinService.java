package tech.elfin;

import lombok.NoArgsConstructor;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import tech.elfin.models.Answer;
import tech.elfin.models.Request;
import tech.elfin.models.RiskFactor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Service
public class ElfinService {

    private static final String ANSWER = "Answer";
    private static final String ADDITIONAL = "Additional";
    private static final String FACTOR = "factor";
    private DmnEngine dmnEngine;
    private DmnDecision decision;

    @Autowired
    public ElfinService(DmnEngine dmnEngine,
                        @Qualifier("dmn_decision") DmnDecision decision) {
        this.dmnEngine = dmnEngine;
        this.decision = decision;
    }

    /**
     * The method validates input data by dmn table.
     *
     * @param request Input data for validating.
     * @return answer with status and additional info, it is used class Answer
     */
    public Answer validate(Request request) {
        Answer answer = new Answer();
        List<String> additions = new ArrayList<>();
        for (RiskFactor riskFactor : request.getRiskFactors()) {
            VariableMap variables = Variables.putValue(FACTOR, riskFactor.getFactor());
            DmnDecisionTableResult result = dmnEngine.evaluateDecisionTable(decision, variables);
            if (result.getSingleResult() != null) {
                answer.setStatus((Boolean) result.getSingleResult().get(ANSWER));
                additions.add((String) result.getSingleResult().get(ADDITIONAL));
                answer.setAdditional(additions);
            }
        }
        return answer;
    }
}
