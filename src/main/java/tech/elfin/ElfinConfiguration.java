package tech.elfin;

import lombok.NoArgsConstructor;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;

@NoArgsConstructor
@Configuration
public class ElfinConfiguration {

    private static final String TEST_TABLE = "TestTable";
    private static final String PATH_TO_DMN = "classpath:dmn/test_task.dmn";
    private ResourceLoader resourceLoader;


    @Autowired
    public ElfinConfiguration(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * Create bean
     *
     * @return new instance of DmnEngine
     */
    @Bean
    public DmnEngine createDmnEngine() {
        DmnEngineConfiguration configuration = DmnEngineConfiguration
                .createDefaultDmnEngineConfiguration();
        return configuration.buildEngine();
    }

    /**
     * Create bean with Qualifier = dmn_decision
     *
     * @return new instance of DmnDecision
     */
    @Bean(name = "dmn_decision")
    public DmnDecision createDecision() {
        Resource resource = resourceLoader.getResource(PATH_TO_DMN);
        InputStream is = null;
        try {
            is = resource.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return createDmnEngine().parseDecision(TEST_TABLE, is);
    }
}
