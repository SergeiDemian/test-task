package tech.elfin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.elfin.models.Answer;
import tech.elfin.models.Request;

@Controller
public class ElfinCtrl {

    private ElfinService elfinService;

    @Autowired
    public ElfinCtrl(ElfinService elfinService) {
        this.elfinService = elfinService;
    }

    @RequestMapping(value = "/scoring", method = RequestMethod.POST)
    @ResponseBody
    public Answer validate(@RequestBody Request request) {
        return elfinService.validate(request);
    }
}
