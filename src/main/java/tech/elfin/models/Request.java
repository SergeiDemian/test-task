package tech.elfin.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
public class Request {

    @Getter
    @Setter
    private String companyName;

    @Getter
    @Setter
    private List<RiskFactor> riskFactors;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;
        Request request = (Request) o;
        return Objects.equals(getCompanyName(), request.getCompanyName()) &&
                Objects.equals(getRiskFactors(), request.getRiskFactors());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCompanyName(), getRiskFactors());
    }
}
