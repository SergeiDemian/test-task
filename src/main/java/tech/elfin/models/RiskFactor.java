package tech.elfin.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@NoArgsConstructor
public class RiskFactor {

    @Getter
    @Setter
    private String factor;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RiskFactor)) return false;
        RiskFactor that = (RiskFactor) o;
        return Objects.equals(getFactor(), that.getFactor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFactor());
    }
}
