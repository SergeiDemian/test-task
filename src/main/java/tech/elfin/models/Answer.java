package tech.elfin.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Answer {

    @Getter
    @Setter
    private boolean status;

    @Getter
    @Setter
    private List<String> additional;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Answer)) return false;
        Answer answer = (Answer) o;
        return isStatus() == answer.isStatus() &&
                Objects.equals(getAdditional(), answer.getAdditional());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isStatus(), getAdditional());
    }
}
